import os, sys
import discord
import asyncio
from discord.ext import commands
from datetime import datetime, timedelta
import requests

# Replace with your Bot Token
TOKEN = ''
# Replace with your Server ID
GUILD_ID = 
# Replace with your Channel ID the bot will post to
CHANNEL_ID = 

# List of websites to check
# Format {'url': 'https://google.com', 'name': 'WebsiteName', 'header': "X-Api-Key: 1234"}
# Added header to pass in auth tokens for status pages if requried 
WEBSITES = [
    {'url': 'https://google.com', 'name': 'Website1'},
    {'url': 'https://google.com', 'name': 'Website2', 'header': "X-Api-Key: 1234"}
]



# Required to make the bot start
def create_bot():
    intents = discord.Intents.default()
    intents.message_content = True
    return commands.Bot(command_prefix='!', intents=intents) 

async def send_notification(client, message):
    channel = client.get_channel(CHANNEL_ID)
    await channel.send(message)
    print("sending message")
    await client.close()

# Check the status of the websites
async def check_websites_status(client):
    status_message = "Website Status:\n"

    #Check the website in last message 
    for website in WEBSITES:
        last_message = await get_last_message(client, website['name'])
        last_status = get_status_from_message(last_message, website['name'])
        
        try:
            headers = website.get('header', {})
            if isinstance(headers, str):
                headers = dict(map(str.strip, pair.split(':', 1)) for pair in headers.split(','))
            print(website['url'])
            response = requests.get(website['url'], headers=headers)
            current_status = response.status_code
            print(current_status)

        except requests.RequestException:
            current_status = None

        if str(current_status) == "200" or str(current_status) == "400" or str(current_status) == "401":
            current_status = "Online"
        else:
            current_status = "Offline"
        

        if current_status is not None and str(current_status) != str(last_status):
            status_message += f'{website["name"]}: {current_status}\n'
        elif current_status is None:
            status_message += f'{website["name"]} is currently unreachable\n'
    #
    print("printing status")
    print(status_message)    

    if status_message != "Website Status:\n":
        await send_notification(client, status_message)
    else:
        await client.close()
#last message 
async def get_last_message(client, website_name):
    channel = client.get_channel(CHANNEL_ID)
    async for message in channel.history(limit=1):
        print(message.content)
        #if website_name in message.content:
        async for message in channel.history(limit=10): 
            if website_name in message.content and message.author.id == client.user.id:
                print(f"{website_name} found in message")
                return message

    return None

def get_status_from_message(message, website):
    if message:
        status_parts = message.content.split('\n')
        for lines in status_parts:
            if "Website Status:" not in lines:
                line = lines.split(':')
                if website == line[0]:
                    return line[1].strip()
    return None

def lambda_handler(event, context):
    client = create_bot()

    @client.event
    async def on_ready():
        print(f'Logged in as {client.user}')
        await check_websites_status(client)

    client.run(TOKEN)

# Uncomment the following line if you want to run the bot locally for testing
#lambda_handler(None, None)
