# Site Monitoring Discord Bot Lambda

## What does it do

I needed a free way to monitor websites and get alerts if they were offline.

This Discord bot will monitor websites and post the status to discord. It uses the last message it sent to see if a status has changed. I am using the AWS "Free Tier" lambda to host this scipt, running every 10 mintues. 

## How to use

Edit the following vars
```
TOKEN = '' - This is the bot token you get from discord
GUILD_ID = - This is your Server ID 
CHANNEL_ID = This is the Channel ID the bot will post in 
WEBSITES = [{}, {}] - The websites that will be checked 
```
Websites can be in one of two formats depending on the if auth is needed
```
Format with auth: {'url': 'https://google.com', 'name': 'WebsiteName', 'header': "X-Api-Key: 1234"}
Format without auth: {'url': 'https://google.com', 'name': 'WebsiteName'}
```

## Upload to Lambda

1. Zip the whole directory as it is
2. Upload to lambda
3. Create a trigger (reccomend event bridge with "rate(10 minutes)")


## Support
Good luck. this will get updated as I need things to be added. feel free to create a pr or fork. 


## License

